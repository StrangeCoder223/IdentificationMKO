﻿using System.Data;

namespace Identification_MKO.Models
{
    public class TrainingSet : Notifier
    {
        public DataTable Data
        {
            get => _data;
            set
            {
                if (value.Rows.Count == 0)
                {
                    IsDataLoaded = false;
                }
                else
                {
                    IsDataLoaded = true;
                }
                _data = value;
                OnPropertyChanged();
            }
        }

        public bool IsDataLoaded
        {
            get => _isDataLoaded;
            private set
            {
                _isDataLoaded = value;
                OnPropertyChanged();
            }
        }
        private bool _isDataLoaded = false;
        private DataTable _data = new DataTable();
    }
}
