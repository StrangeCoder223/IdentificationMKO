﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Identification_MKO.ViewModels
{
    public class ApplicationViewModel : Notifier
    {
        public TrainingSetLoaderVM TrainingSetLoaderVM { get; private set; }
        public TreesVM TreesVM { get; private set; }
        public ResultVM ResultVM { get; private set; }
        

        public ApplicationViewModel()
        {
            TrainingSetLoaderVM = new TrainingSetLoaderVM();
            TreesVM = new TreesVM();
            ResultVM = new ResultVM(TrainingSetLoaderVM, TreesVM);
            TrainingSetLoaderVM.ClickedNextPage += () => TreesVM.Show();
            TreesVM.ClickedNextPage += () => ResultVM.Show();
            ResultVM.ClickedNextPage += () => TrainingSetLoaderVM.Show();
        }
        
    }
}
