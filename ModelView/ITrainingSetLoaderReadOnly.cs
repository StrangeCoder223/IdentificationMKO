﻿using Identification_MKO.Models;

namespace Identification_MKO.ViewModels
{
    public interface ITrainingSetLoaderReadOnly
    {
        public TrainingSet TrainingSet { get;}
    }

    public interface ITreesReadOnly
    {
        public TreesType CurrentTree { get;}
    }
}