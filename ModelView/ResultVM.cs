﻿using Identification_MKO.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Identification_MKO.ViewModels
{
    public class ResultVM : Notifier, IPage
    {
        public event Action ClickedNextPage;
        public Grid InconsistentPageA { get; private set; } = new Grid()
        {
            Visibility = Visibility.Hidden
        };

        public Grid InconsistentPageB { get; private set; } = new Grid()
        {
            Visibility = Visibility.Hidden
        };

        public Matrix FinalMatrix
        {
            get => _finalMatrix;
            private set
            {
                _finalMatrix = value;
                OnPropertyChanged();
            }
        }

        public Matrix LastCriteriasMatrix
        {
            get => _lastCriteriasMatrix;
            private set
            {
                _lastCriteriasMatrix = value;
                OnPropertyChanged();
            }
        }

        public Matrix MiddleCriteriasMatrix
        {
            get => _middleCriteriasMatrix;
            private set
            {
                _middleCriteriasMatrix = value;
                OnPropertyChanged();
            }
        }

        public Matrix FirstCriteriasMatrix
        {
            get => _firstCriteriasMatrix;
            private set
            {
                _firstCriteriasMatrix = value;
                OnPropertyChanged();
            }
        }

        public List<string> Headers
        {
            get => _headers;
            private set
            {
                _headers = value;
                OnPropertyChanged();
            }
        }

        public ICommand ClickAgain
        {
            get
            {
                return new Command((x)=>
                {
                    Hide();
                    ClickedNextPage?.Invoke();
                });
            }
        }
        public ICommand ClickExport
        {
            get
            {
                return new Command((x) =>
                {
                    Export();
                });
            }
        }

        

        private List<string> _headers = new List<string>();
        private ITrainingSetLoaderReadOnly _trainingSetLoader;
        private ITreesReadOnly _trees;
        private int _pointScale;
        private Matrix _finalMatrix;
        private Matrix _middleCriteriasMatrix;
        private Matrix _lastCriteriasMatrix;
        private Matrix _firstCriteriasMatrix;
        private DataTable _baseCarno;
        private DataTable _uniqueValuesTable;
        private int _uniqueValuesCount;
        private List<int> _uniqueNumbers = new List<int>();
        private Dictionary<string, int> _uniqueNumbersDictionary = new Dictionary<string, int>();

        public ResultVM(ITrainingSetLoaderReadOnly trainingSetLoader, ITreesReadOnly trees)
        {
            _trainingSetLoader = trainingSetLoader;
            _trees = trees;
        }

        public void CalculateResult()
        {
            FinalMatrix = GetFinalMatrix();
            switch (_trees.CurrentTree)
            {
                case TreesType.InconsistentA:
                    CalculateTypeA();
                    break;
                case TreesType.InconsistentB:
                    CalculateTypeB();
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void Export()
        {
            List<Matrix> sortedMatrixes = new List<Matrix>();
            sortedMatrixes.Add(FirstCriteriasMatrix);
            sortedMatrixes.Add(MiddleCriteriasMatrix);
            sortedMatrixes.Add(LastCriteriasMatrix);
            sortedMatrixes.Add(FinalMatrix);
            sortedMatrixes.SaveToFile(_trees.CurrentTree);
        }

        private void CalculateTypeA()
        {
            MiddleCriteriasMatrix = GetMatrixInLeftSide(FinalMatrix.Carno);
            LastCriteriasMatrix = GetMatrixInRightSide(MiddleCriteriasMatrix.Carno);
            FirstCriteriasMatrix = GetMatrixInLeftSide(MiddleCriteriasMatrix.Carno);
        }

        private void CalculateTypeB()
        {
            LastCriteriasMatrix = GetMatrixInRightSide(FinalMatrix.Carno);

            MiddleCriteriasMatrix = GetMatrixInLeftSide(LastCriteriasMatrix.Carno);

            FirstCriteriasMatrix = GetMatrixInLeftSide(MiddleCriteriasMatrix.Carno);
        }

        private Matrix GetMatrixInLeftSide(DataTable carno)
        {
            DataTable newCarno = GetTableOnRowsEnumeration(carno, _uniqueNumbers);
            DataTable newCarnoWithoutDuplicates = DeleteDuplicateRows(newCarno);
            return new Matrix(newCarnoWithoutDuplicates, newCarno);
        }

        private Matrix GetMatrixInRightSide(DataTable carno)
        {
            if (carno.Columns.Count == _uniqueValuesCount)
            {
                carno = TransposeTable(carno);
            }
            DataTable uniqueValuesTable = DeleteDuplicateRows(carno);
            DataColumn[] columnsOrder = GetColumnsOrderByTable(uniqueValuesTable);
            DataTable sortedUniqueValues = SortTableByColumn(columnsOrder, uniqueValuesTable); //сортируем, чтобы пронумеровать и вывести x4+x5
            DataTable result = DeleteDuplicateRows(GetTableOnRowsEnumeration(TransposeTable(sortedUniqueValues), _uniqueNumbers));
           

            return new Matrix(result, carno);
        }

        private Matrix GetFinalMatrix()
        {
            DataTable baseTable = _trainingSetLoader.TrainingSet.Data;
            _uniqueNumbers = GetUniqueNumbersFromTable(baseTable);
            DataColumn[] columnsOrder = GetColumnsOrderByTable(baseTable);
            List<string> headers = new List<string>();
            foreach (var column in columnsOrder)
            {
                headers.Add(column.ColumnName);
            }
            Headers = headers;
            DataTable sortedTable = SortTableByColumn(columnsOrder, baseTable);
            string tableContent = string.Empty;
            int maxNumber = 0;
            for (int i = 0; i < sortedTable.Rows.Count; i++)
            {
                string array = string.Empty;
                for (int j = 0; j < sortedTable.Rows[i].ItemArray.Length; j++)
                {
                    array += sortedTable.Rows[i].ItemArray[j].ToString();
                }
                tableContent += array;
            }
            for (int i = 0; i < int.MaxValue; i++)
            {
                if (tableContent.Contains(i.ToString()))
                {
                    maxNumber++;
                }
                else
                {
                    break;
                }
            }


            _pointScale = (int)MathF.Pow(2, maxNumber); // step 2 - end
            sortedTable = DeleteDuplicateRows(sortedTable); // step 3 - end
            _baseCarno = CreateCarnoBasedOnVerticalColumn(sortedTable, sortedTable.Columns.Count - 1); // step 4 - end
            InitializeUniqueNumbersDictionary(_baseCarno, _uniqueNumbers);
            _uniqueValuesTable = DeleteDuplicateRows(_baseCarno); // step 5 - end
            _uniqueValuesCount = _uniqueValuesTable.Rows.Count;

            DataTable transposedCarno = TransposeTable(_uniqueValuesTable);

            return new Matrix(DeleteDuplicateRows(transposedCarno), _baseCarno);
        }

        private DataColumn[] GetColumnsOrderByTable(DataTable columnsTable)
        {
            List<DataColumn> columns = new List<DataColumn>();
            for (int i = 0; i < columnsTable.Columns.Count; i++)
            {
                columns.Add(columnsTable.Columns[i]);
            }
            return columns.ToArray();
        }

        private void InitializeUniqueNumbersDictionary(DataTable baseCarno, List<int> uniqueNumbers)
        {
            DataTable sortedBaseCarno = SortTableByColumn(GetColumnsOrderByTable(baseCarno), baseCarno);
            _uniqueNumbersDictionary.Clear();
            int currentUniqueNumbersIndex = 0;
            for (int i = 0; i < sortedBaseCarno.Rows.Count; i++)
            {
                if (i == 0)
                {
                    _uniqueNumbersDictionary.Add(sortedBaseCarno.Rows[i].ItemArray.CastToString(), uniqueNumbers[currentUniqueNumbersIndex]);
                    continue;
                }
                if (sortedBaseCarno.Rows[i].ItemArray.CastToString() != sortedBaseCarno.Rows[i - 1].ItemArray.CastToString())
                {
                    currentUniqueNumbersIndex++;
                    _uniqueNumbersDictionary.Add(sortedBaseCarno.Rows[i].ItemArray.CastToString(), uniqueNumbers[currentUniqueNumbersIndex]);
                }
                
            }
        }

        private DataTable GetTableOnRowsEnumeration(DataTable dataTable, List<int> uniqueNumbers)
        {
            InitializeUniqueNumbersDictionary(dataTable, uniqueNumbers);
            List<int> enumeration = new List<int>();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                enumeration.Add(_uniqueNumbersDictionary[dataTable.Rows[i].ItemArray.CastToString()]);
            }
            return TransposeEnumeration(enumeration, uniqueNumbers.Count);
        }

        private List<int> GetUniqueNumbersFromTable(DataTable baseTable)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < baseTable.Rows.Count; i++)
            {
                for (int j = 0; j < baseTable.Rows[i].ItemArray.Length; j++)
                {
                    result.Add(Convert.ToInt32(baseTable.Rows[i].ItemArray[j]));
                }
            }
            result = result.Distinct().ToList();
            result.Sort();
            return result;
        }

        private DataTable FillColumns(int columnsCount)
        {
            DataTable newTable = new DataTable();
            for (int i = 0; i < columnsCount; i++)
            {
                newTable.Columns.Add(i.ToString());
            }
            return newTable;
        }

        private DataTable CreateCarnoBasedOnVerticalColumn(DataTable table, int columnIndexBasedOn)
        {
            DataTable carno = new DataTable();
            for (int k = 0; k < _pointScale; k++)
            {
                carno.Columns.Add(k.ToString());
            }
            for (int i = 0; i < table.Rows.Count/_pointScale; i++)
                {
                    List<object> values = new List<object>();
                    for (int j = i*_pointScale; j < i*_pointScale+_pointScale; j++)
                    {
                        values.Add(table.Rows[j].ItemArray[columnIndexBasedOn]);
                    }
                    carno.Rows.Add(values.ToArray());
                }
            return carno;
        }

        private DataTable TransposeEnumeration(List<int> enumeration, int uniqueNumbers)
        {
            DataTable newTable = new DataTable();
            int columnsCount = uniqueNumbers; //ИСПРАВИТЬ!!! КОЛ-ВО КОЛОНОК НЕПРАВИЛЬНО СЧИТАЕТСЯ
            newTable = FillColumns(columnsCount);
            for (int i = 0; i < enumeration.Count; i+=columnsCount)
            {
                object[] row = new object[columnsCount];
                for (int j = 0; j < columnsCount; j++)
                {
                    row[j] = enumeration[i+j];
                }
                newTable.Rows.Add(row);
            }

            return newTable;
        }

        private DataTable TransposeTable(DataTable matrix)
        {
            DataTable transposed = new DataTable();
            for (int k = 0; k < matrix.Rows.Count; k++)
            {
                transposed.Columns.Add(k.ToString());
            }
            for (int c = 0; c < matrix.Columns.Count; c++)
            {
                List<object> values = new List<object>();
                for (int i = 0; i < matrix.Rows.Count; i++)
                {
                    values.Add(matrix.Rows[i].ItemArray[c]);
                }
                transposed.Rows.Add(values.ToArray());
            }
            return transposed;
        }

        private DataTable SortTableByColumn(DataColumn[] orderSort, DataTable table)
        {
            var sorted = table.AsEnumerable().OrderBy(x => x.Field<string>(orderSort[0].ColumnName));
            for (int i = 1; i < orderSort.Length; i++)
            {
                if (i >= table.Columns.Count)
                {
                    break;
                }
                sorted.ThenBy(r => r.Field<string>(orderSort[i].ColumnName));
            }
            return sorted.CopyToDataTable();
        }

        private DataTable DeleteDuplicateRows(DataTable table)
        {
            DataTable newTable = new DataTable();
            newTable = FillColumns(table.Columns.Count);
            List<object?[]> rowsData = table.Rows.CastToList();
            List<string> rowsDataInString = new List<string>();
            List<string> rowsDataInStringWithoutDuplicates = new List<string>();
            for (int i = 0; i < rowsData.Count; i++)
            {
                rowsDataInString.Add(rowsData[i].CastToString());
            }

            List<object?[]> rowsDataWithoutDuplicates = new List<object?[]>();
            for (int i = 0; i < rowsDataInString.Count; i++)
            {
                if (!rowsDataInStringWithoutDuplicates.Contains(rowsDataInString[i]))
                {
                    rowsDataInStringWithoutDuplicates.Add(rowsDataInString[i]);
                    rowsDataWithoutDuplicates.Add(rowsData[i]);
                }
            }

            foreach (var data in rowsDataWithoutDuplicates)
            {
                newTable.Rows.Add(data);
            }

            return newTable;
        }

        public void Hide()
        {
            switch (_trees.CurrentTree)
            {
                case TreesType.InconsistentA:
                    InconsistentPageA.Visibility = Visibility.Hidden;
                    break;
                case TreesType.InconsistentB:
                    InconsistentPageB.Visibility = Visibility.Hidden;
                    break;
            }
        }

        public void Show()
        {
            switch (_trees.CurrentTree)
            {
                case TreesType.InconsistentA:
                    InconsistentPageA.Visibility = Visibility.Visible;
                    break;
                case TreesType.InconsistentB:
                    InconsistentPageB.Visibility = Visibility.Visible;
                    break;
            }
            CalculateResult();
        }
    }

    
}

public static class Extensions
{
    public static string CastToString(this object?[] value)
    {
        string result = string.Empty;
        foreach (var obj in value)
        {
            result += obj?.ToString();
        }
        return result;
    }

    public static List<object?[]> CastToList(this DataRowCollection value)
    {
        List<object?[]> newList = new List<object?[]>();
        for (int i = 0; i < value.Count; i++)
        {
            newList.Add(value[i].ItemArray);
        }
        return newList;
    }

    public static void Debug(this DataTable value)
    {
        string result = string.Empty;
        for (int i = 0; i < value.Rows.Count; i++)
        {
            for (int j = 0; j < value.Columns.Count; j++)
            {
                result += value.Rows[i].ItemArray[j].ToString();

            }
            result += "\n";
        }
        MessageBox.Show(result);
    }

    private static List<string> GetTitlesByTree(TreesType treeType)
    {
        List<string> titles = new List<string>();
        switch (treeType)
        {
            case TreesType.InconsistentA:
                titles = new List<string>()
                {
                    "'Критерий 1'".Replace("'", "\""),
                    "'Критерий 2'".Replace("'", "\""),
                    "'Критерий 3'".Replace("'", "\""),
                    "'Критерий 4'".Replace("'", "\""),
                    "(Матрица 1)",
                    "(Матрица 2)",
                    "(Матрица 3)",
                    "'Критерий 5'".Replace("'", "\"")
                };
                break;
            case TreesType.InconsistentB:
                titles =  new List<string>()
                {
                    "'Критерий 1'".Replace("'", "\""),
                    "'Критерий 2'".Replace("'", "\""),
                    "(Матрица 1)",
                    "'Критерий 3'".Replace("'", "\""),
                    "'Критерий 4'".Replace("'", "\""),
                    "'Критерий 5'".Replace("'", "\""),
                    "(Матрица 2)",
                    "(Матрица 3)",
                };
                break;
        }
        return titles;
    }

    public static void SaveToFile(this List<Matrix> sortedMatrixes, TreesType tree)
    {
        StringBuilder fileContent = new StringBuilder();
        SaveFileDialog saveFileDialog = new SaveFileDialog();
        string path = string.Empty;
        List<string> titles = GetTitlesByTree(tree);
        saveFileDialog.Filter = "CSV Files (*.csv)|*.csv";
        if (saveFileDialog.ShowDialog() == true)
        {
            path = saveFileDialog.FileName;
        }
        int titleIndex = 0;
        for (int i = 0; i < sortedMatrixes.Count; i++)
        {
            fileContent.Append(titles[titleIndex] + ";" + titles[titleIndex+1] + "\n");
            titleIndex += 2;
            foreach (DataRow dr in sortedMatrixes[i].Data.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    fileContent.Append(column.ToString() + ";");
                }

                fileContent.Replace(";", System.Environment.NewLine, fileContent.Length - 1, 1);
            }
        }
        

        System.IO.File.WriteAllText(path, fileContent.ToString());
    }

   
}