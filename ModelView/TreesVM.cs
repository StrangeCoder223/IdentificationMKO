﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Identification_MKO.ViewModels
{
    public class TreesVM : Notifier, IPage, ITreesReadOnly
    {
        public event Action ClickedNextPage;
        public Grid Page { get; private set; } = new Grid()
        {
            Visibility = Visibility.Hidden
        };

        public ICommand ClickChooseTree
        {
            get
            {
                return new Command((x) =>
                {
                    CurrentTree = (TreesType)Enum.Parse(typeof(TreesType), x.ToString());
                });
            }
        }

        public ICommand ClickNext
        {
            get
            {
                return new Command((x =>
                {
                    Page.Visibility = Visibility.Hidden;
                    ClickedNextPage?.Invoke();
                }));
            }
        }

        public TreesType CurrentTree { get; private set; }

        public void Show()
        {
            Page.Visibility = Visibility.Visible;
        }

        public void Hide()
        {
            Page.Visibility = Visibility.Hidden;
        }
    }

    public enum TreesType
    {
        InconsistentA,
        InconsistentB
    }
}