﻿using Identification_MKO.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Identification_MKO.ViewModels
{
    public class TrainingSetLoaderVM : Notifier, IPage, ITrainingSetLoaderReadOnly
    {
        public event Action ClickedNextPage;
        public Grid Page { get; private set; } = new Grid();
        public TrainingSet TrainingSet { get; private set; } = new TrainingSet();
        public int LoadInPercents
        {
            get => _loadInPercents;
            private set
            {
                _loadInPercents = value;
                OnPropertyChanged();
            }
        }
        public ICommand ClickLoadSet
        {
            get
            {
                return new Command((x) =>
                {
                    Load();
                });
            }
        }
        public ICommand ClickNext
        {
            get
            {
                return new Command((x =>
                {
                    Hide();
                    ClickedNextPage?.Invoke();
                }));
            }
        }

        private CSVReader _reader;
        private int _loadInPercents;

        public TrainingSetLoaderVM()
        {
            _reader = new CSVReader(string.Empty);
        }

        private void UpdateProgress(int percent)
        {
            LoadInPercents += percent;
        }

        private void Load()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV Files (*.csv)|*.csv";
            if (dialog.ShowDialog() == true)
            {
                ResetLoadProgress();
                DataTable table = ConvertCSVtoDataTable(dialog.FileName); 
                TrainingSet.Data = table;
            }
        }

        private void ResetLoadProgress()
        {
            TrainingSet.Data = new DataTable();
            LoadInPercents = 0;
        }

        public void Show()
        {
            Page.Visibility = Visibility.Visible;
            ResetLoadProgress();
        }

        public void Hide()
        {
            Page.Visibility = Visibility.Hidden;
        }

        private DataTable ConvertCSVtoDataTable(string filePath)
        {
            DataTable dt = new DataTable();
            
            using (StreamReader stream = new StreamReader(filePath))
            {
                string[] headers = stream.ReadLine().Split(';');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!stream.EndOfStream)
                {
                    string[] rows = stream.ReadLine().Split(';');
                    int percent = 100 / (dt.Columns.Count * rows.Length);
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                        UpdateProgress(percent);
                    }
                    dt.Rows.Add(dr);
                }

            }

            return dt;
        }
    }
}
