﻿using System;

namespace Identification_MKO.ViewModels
{
    public interface IPage
    {
        public event Action ClickedNextPage;
        void Show();
        void Hide();

    }
}
