﻿using Identification_MKO;
using System.Data;
using System.Windows;

public class Matrix : Notifier
{
    public string Content
    {
        get => _content;
        private set
        {
            _content = value;
            OnPropertyChanged();
        }
    }
    
    public DataTable Carno { get; private set; }
    public DataTable Data { get; private set; }
    private int[,] _array;
    private string _content = string.Empty;

    public Matrix(DataTable table, DataTable carnoOnWhichThisBuilt)
    {
        Carno = carnoOnWhichThisBuilt;
        Data = table;
        _array = new int[table.Rows.Count, table.Columns.Count];
        FillArray(table);
    }

    private void FillArray(DataTable table)
    {
        string result = string.Empty;
        for (int i = 0; i < table.Rows.Count; i++)
        {
            for (int j = 0; j < table.Columns.Count; j++)
            {
                _array[i, j] = int.Parse(table.Rows[i].ItemArray[j].ToString());
                result += _array[i, j].ToString();
            }
            result += "\n";
        }
        Content = ConvertArrayToTextView();
    }

    private string ConvertArrayToTextView()
    {
        string result = string.Empty;
        int rows = _array.GetUpperBound(0) + 1;
        int columns = _array.Length / rows;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                result += _array[i, j].ToString();
            }
            result += "\n";
        }
        
        return result;
    }
 }
