﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Identification_MKO
{
    public class Notifier: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }

}